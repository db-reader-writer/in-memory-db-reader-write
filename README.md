# In Memory DB Reader Write
A simple and lightweight Spring Boot API that can manipulate a in memory database and expose these manipulations via API

## How To
- [ ] Code it
- [ ] Open a Merge Request with your code
- [ ] Enjoy.

## TODO 

- [ ] Spring Boot Application that includes an In Memory Database
- [ ] CRUD API to operate directly on the database
- [ ] Unit Tests

## Bonus
- [ ] Gitlab-ci pipeline that executes unittests
